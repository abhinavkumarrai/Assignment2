//Each Function

function each(item, cb){
    if(Array.isArray(item) && typeof cb === 'function'){
        for(let i = 0;i < item.length; i++){
            cb(item[i], i, item);
        }
    }
    else{
        return 0;
    }
}

module.exports = each;



//Map Function

let arr =[];
function map(element, cb){
    if(Array.isArray(element) && typeof cb === 'function'){
        for(let i = 0; i < element.length; i++){
            n.push(cb(element[i], i, element));
        }
        return arr;
    }
    else{
        return arr;
    }
}

module.exports = map;

//End of Map Function
//---------------------------------------------------------------------

//Reduce Function

function reduce(elements, cb, startingvalue){
    if(Array.isArray(elements) && typeof cb === 'function'){
        let b = startingvalue === undefined ? 0:startingvalue;
        for(let i = 0; i < elements.length; i++){
            b = cb(b, elements[i]);
            console.log(b,i);
        }
        return b;
    }
    else{
        return 0;
    }
}

module.exports = reduce;



//Find Function

function find(elements, cb){
    if(Array.isArray(elements) && typeof cb === 'function'){
        for(let i = 0; i < elements.length; i++){
           if(cb(elements[i])){
               return elements[i];
           }
        }
    }
}

module.exports = find;


//Filter function

let ar = [];
function filter(elements,cb){
    if(Array.isArray(elements) && typeof cb === 'function'){
        for(let i = 0; i < elements.length; i++){
            if(cb(elements[i])){
                ar.push(elements[i]);
            }
        }
        return ar;
    }
    else{
        return ar;
    }
}

module.exports = filter;



//Flatten Function

let arr1 = [];
function flat(item){
    if(Array.isArray(item)){
        for(let i = 0; i < item.length; i++){
            if(!Array.isArray(item[i])){
                arr1.push(item[i]);
            }
            if(Array.isArray(item[i])){
                for(let j = 0; j < item.length; j++){
                    flat(item[j]);
                }
            }
        }
        return arr1;
    }
    else{
        return arr1;
    }
}

module.exports = flat;


