//Function counterFactory

function counterFactory(){
    let cnt = 0;
    const obj = {

        increment(){
            return ++cnt;
        },
        
        decrement(){
            return --cnt;
        }
    };
    return obj;
}

module.exports = counterFactory;

//Function limitFunctionCallCount(cb, n)

function limitFunctionCallCount(cb, n){
    let cnt = 0;
    function count(){
        if(cnt < n && typeof cb === 'function'){
            cnt++;
            return cb();
        }
        else{
            return null;
        }
    }
    return count;
}

module.exports = limitFunctionCallCount;

